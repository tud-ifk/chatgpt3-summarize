[![version](https://kartographie.geo.tu-dresden.de/ad/2022-12-22_OpenAI_Summary/version.svg)][static-gl-url] [![pipeline](https://kartographie.geo.tu-dresden.de/ad/2022-12-22_OpenAI_Summary/pipeline.svg)][static-gl-url]

# Using GPT-3 to summarize text via API

The notebook converted to HTML can be found [here][1].

[1]: https://kartographie.geo.tu-dresden.de/ad/2022-12-22_OpenAI_Summary/html/gpt3-summary.html
[static-gl-url]: https://gitlab.hrz.tu-chemnitz.de/tud-ifk/chatgpt3-summarize
